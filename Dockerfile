FROM openjdk:8-jre-alpine

RUN mkdir /app

COPY /target/api-0.0.1-SNAPSHOT.jar /app/app.jar

WORKDIR /app

EXPOSE 8081

ENTRYPOINT ["java", "-jar", "app.jar"]