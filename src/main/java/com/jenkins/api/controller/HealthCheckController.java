package com.jenkins.api.controller;

import com.jenkins.api.dto.RestResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
@RequestMapping("/health-check")
@Slf4j
public class HealthCheckController {

    @GetMapping
    public RestResponseDto index(HttpServletRequest request) throws UnknownHostException {
        log.info("Received request from ip address: {}", request.getRemoteAddr());
        RestResponseDto response = new RestResponseDto();
        response.setMessage("Successfully received the response!");
        InetAddress localhost = InetAddress.getLocalHost();
        response.setData(localhost);
        return response;
    }
}
