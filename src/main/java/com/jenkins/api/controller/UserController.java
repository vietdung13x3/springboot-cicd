package com.jenkins.api.controller;

import com.jenkins.api.dto.UserDto;
import com.jenkins.api.entity.User;
import com.jenkins.api.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getUsers(HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        log.info("Received request to get users with ip: {}", ip);
        return userService.getAllUsers();
    }
    @PostMapping
    public User addUser(HttpServletRequest request, @Valid @RequestBody UserDto createdUser) {
        String ip = request.getRemoteAddr();
        log.info("Received request to add user with ip: {}", ip);
        return userService.createUser(createdUser);
    }

    @PutMapping("{id}")
    public User updateUser(@PathVariable Long id, HttpServletRequest request, @RequestBody UserDto updatedUser) {
        String ip = request.getRemoteAddr();
        log.info("Received request to update user with ip: {}", ip);
        return userService.updateUser(id, updatedUser);
    }

    @DeleteMapping("{id}")
    public User deleteUser(@PathVariable Long id, HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        log.info("Received request to update user with ip: {}", ip);
        return userService.deleteUser(id);
    }
}
