package com.jenkins.api.dto;

import lombok.Data;

@Data
public class RestResponseDto<T> {
    private String message;
    private String requestId;
    private T data;
}
