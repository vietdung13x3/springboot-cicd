package com.jenkins.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @NotEmpty
    private String firstname;
    @NotEmpty
    private String lastname;
    @Min(value = 18)
    private Integer age;
}
