package com.jenkins.api.service;

import com.jenkins.api.dto.UserDto;
import com.jenkins.api.entity.User;
import com.jenkins.api.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers() {
        log.info("Get all user from the database");
        return userRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public User createUser(UserDto createdUser) {
        User user = new User();
        user.setAge(createdUser.getAge());
        user.setFirstname(createdUser.getFirstname());
        user.setLastname(createdUser.getLastname());
        user.setCreatedDate(new Date());
        log.info("Saved user at: {}", user.getCreatedDate().toString());
        return userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public User updateUser(Long id, UserDto updatedUser) {
        Optional<User> userWrapper = userRepository.findById(id);
        if(!userWrapper.isPresent()) {
            log.error("User has not been found from database with id: {}", id);
            throw new RuntimeException("User has not been found with id: " + id);
        }
        User savedUser = userWrapper.map(user -> {
            user.setAge(updatedUser.getAge());
            user.setFirstname(updatedUser.getFirstname());
            user.setLastname(updatedUser.getLastname());
            return user;
        }).get();
        log.info("Successfully updated user with id: {}", savedUser.getId());
        return userRepository.save(savedUser);
    }

    @Transactional(rollbackFor = Exception.class)
    public User deleteUser(Long id) {
        Optional<User> userWrapper = userRepository.findById(id);
        if(!userWrapper.isPresent()) {
            log.error("User has not been found from database with id: {}", id);
            throw new RuntimeException("User has not been found with id: " + id);
        }
        userRepository.deleteById(id);
        log.info("Successfully deleted user with id: {}", userWrapper.get().getId());
        return userWrapper.get();
    }
}
