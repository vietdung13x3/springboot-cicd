package com.jenkins.api.service;

import com.jenkins.api.dto.UserDto;
import com.jenkins.api.entity.User;
import com.jenkins.api.repository.UserRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;

    private User user;
    private User additionalUser;

    @BeforeEach
    void setUp() {
        this.user = new User();
        this.additionalUser = new User();
        this.user.setFirstname("Firstname");
        this.user.setLastname("Lastname");
        this.user.setAge(20);
        this.user.setCreatedDate(new Date());
        this.user.setId(1L);
        this.additionalUser.setFirstname("Firstname-Additional");
        this.additionalUser.setLastname("Lastname-Additional");
        this.additionalUser.setAge(20);
        this.additionalUser.setCreatedDate(new Date());
        this.additionalUser.setId(2L);
    }

    @Test
    @DisplayName("Service: Test get all users - return list of users")
    public void getAllUsers_shouldReturnListOfUsers() {
        Mockito.when(userRepository.findAll())
                .thenReturn(Arrays.asList(user, additionalUser));

        List<User> res = userService.getAllUsers();

        Assertions.assertEquals(2, res.size());
    }

    @Test
    @DisplayName("Service: Test save user - return saved user")
    public void saveUser_shouldReturnSavedUser() {
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);

        UserDto dto = UserDto.builder()
                .age(20)
                .firstname("Firstname")
                .lastname("Lastname")
                .build();

        User savedUser = userService.createUser(dto);
        Assertions.assertEquals("Firstname", savedUser.getFirstname());
        Assertions.assertEquals("Lastname", savedUser.getLastname());
        Assertions.assertEquals(20, savedUser.getAge());
    }

    @Test
    @DisplayName("Service: Test update user - return updated user")
    public void updateUser_shouldReturnUpdatedUser() {
        Mockito.when(userRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.of(user));
        user.setFirstname("Updated-Firstname");
        user.setLastname("Updated-Lastname");
        user.setAge(30);
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(user);

        User updatedUser = userService.updateUser(12L, UserDto.builder()
                .age(20)
                .firstname("Updated-Firstname")
                .age(30)
                .lastname("Updated-Lastname")
                .build()
        );
        Assertions.assertEquals(user.getFirstname(), updatedUser.getFirstname());
        Assertions.assertEquals(user.getLastname(), updatedUser.getLastname());
        Assertions.assertEquals(user.getAge(), updatedUser.getAge());
    }

    @Test
    @DisplayName("Service: Test delete user - return deleted user")
    public void deleteUser_shouldReturnDeletedUser() {
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        User deletedUser = userService.deleteUser(1L);
        Assertions.assertEquals(1L, deletedUser.getId());
    }

    @Test
    @DisplayName("Service: Test delete user - throw error")
    void testDeleteUser_notFound() {
        // Arrange
        Mockito.when(userRepository.findById(1L))
                .thenReturn(Optional.empty());

        // Act and Assert
        Assertions.assertThrows(
                RuntimeException.class, () -> {
                    userService.deleteUser(1L);
                }
        );
    }
}
